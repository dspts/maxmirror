# Calculate the maximum length of mirror portions of a list.

This is a way I came up with, while trying to solve the problem of max mirror length of a list. 
The mirror portions are ones that map the same if we look at the list either way.

As an example, if we have the following list:
```
[1, 2, 3, 4, 2, 1]
```

the mirror portion is ```[1,2]``` so the maximum length is 2.

While trying to solve this problem, I constructed the following table, where in bold are the regions of interest:
Upon constructing the table, I just search across multiple diagonals of each row for ```True``` elements.

* For ```[1, 2, 3, 4, 2, 1]```

| - | 1 | 2 | 4 | 3 | 2 | 1 |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | ``T`` | F | F | F | F | T |
| 2 | F | ``T`` | F | F | T | F |
| 3 | F | F | F | T | F | F |
| 4 | F | F | T | F | F | F |
| 2 | F | T | F | F | T | F |
| 1 | T | F | F | F | F | T |

Similar examples are shown, below:

* For ```[4, 3, 1, 2, 2, 1]```:

| - | 4 | 3 | 1 | 2 | 2 | 1 |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 1 | F | F | ``T`` | F | F | T |
| 2 | F | F | F | ``T`` | T | F |
| 2 | F | F | F | T | ``T`` | F |
| 1 | F | F | T | F | F | ``T`` |
| 3 | F | T | F | F | F | F |
| 4 | T | F | F | F | F | F |

* For ```[1, 2, 2, 4]```:

| - | 1 | 2 | 2 | 4 |
|:-:|:-:|:-:|:-:|:-:|
| 4 | F | F | F | T |
| 2 | F | ``T`` | T | F |
| 2 | F | T | ``T`` | F |
| 1 | T | F | F | F |

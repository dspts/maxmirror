def maxMirror(li):
    """
    Calculate maximum list mirror length.
    """
    maxmirror = 1   # maxmirror length
    for start in range(len(li) - 1):
        iter1 = iter(li[start:])
        iter2 = reversed(li[start:])
        counter = 0 # temporary var to check subsequence lengths
        try:
            while True:
                a = next(iter1)
                while (a != (b := next(iter2))):
                    maxmirror = counter if counter > maxmirror else maxmirror
                counter += 1
        except StopIteration:
            maxmirror = counter if counter > maxmirror else maxmirror
    print(f"maxmirror length of {li} : {maxmirror}")

if __name__ == "__main__":
    li = [1,2,3,2,1,4,4,5,6,6,5,4]
    maxMirror(li)

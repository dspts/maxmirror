def buildArr(array):
    """
    Build boolean matrix.
    """
    boolarr = [[x == y for x in array[::-1]] for y in array]
    for row in boolarr:
        print(row)
    return boolarr


def searchDiag(searcharr, row, col):
    """
    Search through a given diagonal, starting from (row,col) of matrix.
    If successive diagonal elements are different, stop the search.
    """
    searchmax = 1
    print("Enter search for: " + str((row, col)))
    while (row < len(searcharr[0]) - 1) and (col < len(searcharr[0]) - 1):
        print("Compared " + str((row, col)) + " with " + str((row + 1, col + 1)))
        if searcharr[row][col] and searcharr[row + 1][col + 1]:
            searchmax += 1
            print("searchmax: " + str(searchmax))
        else:
            break
        row, col = row + 1, col + 1
    print("returned searchmax: " + str(searchmax))
    return searchmax

def maxMirror(array):
    """
    Search through all diagonals.
    """
    best_max = 1
    for row in range(0, len(array) - 2):
        for col in range(0, len(array) - 1):
            temp_max = searchDiag(array, row, col)
            best_max = temp_max if best_max < temp_max else best_max
    return best_max

if __name__ == '__main__':
    arr = [1, 2, 1, 0, 2, 1]
    print(maxMirror(buildArr(arr)))

def maxMirror3(li):
    """
    Return the max mirror portion of a list.
    For example, if li = [1,2,3,4,5,2,1] the max mirror portion is [1, 2] so the length is 2.
    """

    max_length = 1
    for cnt1 in range(len(li)):
        res = 0
        for cnt2 in range(len(li)-1,cnt1-1,-1):
            if li[cnt1 + res] == li[cnt2]:
                res += 1
                max_length = res if res > max_length else max_length
            else:
                max_length = res if res > max_length else max_length
                res = 0
    return max_length

if __name__ == '__main__':
    li = [1, 1, 1, 1, 1]
    print(f"Max mirror length of {li} is {maxMirror2(li)}")
